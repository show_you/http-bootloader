/**
  ******************************************************************************
  * @file    LwIP/LwIP_HTTP_Server_Netconn_RTOS/Src/main.c 
  * @author  MCD Application Team
  * @brief   This sample code implements a http server application based on 
  *          Netconn API of LwIP stack and FreeRTOS. This application uses 
  *          STM32F7xx the ETH HAL API to transmit and receive data. 
  *          The communication is done with a web browser of a remote PC.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2016 STMicroelectronics International N.V. 
  * All rights reserved.</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
//#include "cmsis_os.h"
#include "ethernetif.h"
#include "lwip/opt.h"
#include "lwip/netif.h"
//#include "lwip/tcpip.h"
#include "lwip/init.h"
#include "lwip/timeouts.h"
#include "netif/etharp.h"
//#include "app_ethernet.h"
#include "httpserver-netconn.h"
#include "eeprom.h"

/* Private variables ---------------------------------------------------------*/

SDRAM_HandleTypeDef hsdram1;

typedef  void (*pFunction)(void);

pFunction Jump_To_Application;
uint32_t JumpAddress;

/* Declaration of task handlers */
osThreadId LwIPTaskHandle;

/* declaring net interface */
struct netif gnetif;

UART_HandleTypeDef huart6;

/* variable defines the state for LWIP task
 * if set to 0 - need first initialization (before first start or after IP change)
 * if set to 1 - means ordinary work of task
 * if set to 2 - means state for waiting of ethernet cable connection
 */


/* array of memory address for eeprom-saved values */
uint16_t VirtAddVarTab[NB_OF_VAR] = {
		0x0001,
		0x0889,
		0x1111,
		0x1999,
		0x2221,
		0x2AA9,
		0x3331,
		0x3BB9,
		0x4441,
		0x4CC9,
		0x5551,
		0x5DD9,
		0x6661,
		0x6EE9,
		0x7771,
		0x7FF9,
		0x8881,
		0x9109,
		0x9991,
		0xA219,
		0xAAA1,
		0xB329,
		0xBBB1,
		0xC439,
		0xCCC1,
		0xD549,
		0xDDD1,
		0xE659,
		0xEEE1,
		0xF769
};

/* initialization of intermediate flash-save-needed variables */
uint16_t VarDataTab[NB_OF_VAR] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};


uint16_t EE_status = 0;

uint8_t ipValue[4];
uint8_t maskValue[4];


/* Private function prototypes -----------------------------------------------*/
static void SystemClock_Config(void);
//static void BSP_Config(void);
static void Netif_Config(void);
static void MPU_Config(void);
static void Error_Handler(void);
static void CPU_CACHE_Enable(void);
static void MX_USART6_UART_Init(void);
static void MX_GPIO_Init(void);
FirmwareLoadMode EE_readLoadMode(void);
void EE_writeLoadMode(FirmwareLoadMode loadMode);



/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{

	FirmwareLoadMode loadMode = BOOTLOADER_MODE;

	  /* Configure Key Button */
	  BSP_PB_Init(BUTTON_KEY, BUTTON_MODE_GPIO);

		/* Configure the MPU attributes as Device memory for ETH DMA descriptors */
	  MPU_Config();

	  /* Enable the CPU Cache */
	  CPU_CACHE_Enable();

	  /* STM32F7xx HAL library initialization:
	       - Configure the Flash ART accelerator on ITCM interface
	       - Configure the Systick to generate an interrupt each 1 msec
	       - Set NVIC Group Priority to 4
	       - Global MSP (MCU Support Package) initialization
	     */
	  HAL_Init();

	  /* Configure the system clock to 200 MHz */
	  SystemClock_Config();

	  HAL_FLASH_Unlock();

//	  MX_USART6_UART_Init();
	  MX_GPIO_Init();

	  EE_Init();

	  loadMode = EE_readLoadMode();

	  uint16_t status = HAL_OK;

	  for (uint8_t i = 8; i < 16; i++) {
		  status = EE_ReadVariable(VirtAddVarTab[i],  &VarDataTab[i]);
		  if (status == HAL_OK)
		  {
			  if (i < 12)
			  {
				  ipValue[i - 8] = VarDataTab[i];
			  } else
			  {
				  maskValue[i - 12] = VarDataTab[i];
			  }
		  } else
		  {
			  ipValue[0] = IP_ADDR0;
			  ipValue[1] = IP_ADDR1;
			  ipValue[2] = IP_ADDR2;
			  ipValue[3] = IP_ADDR3;
			  maskValue[0] = NETMASK_ADDR0;
			  maskValue[1] = NETMASK_ADDR1;
			  maskValue[2] = NETMASK_ADDR2;
			  maskValue[3] = NETMASK_ADDR3;
			  break;
		  }
	  }

	  /* Test if Key push-button is not pressed */
	  if ((BSP_PB_GetState(BUTTON_KEY) == 0x00) && (loadMode == MAIN_FIRMWARE_MODE || loadMode == MAIN_FIRMWARE_TEST_MODE))
	  { /* Key push-button not pressed: jump to user application */

		  if (loadMode == MAIN_FIRMWARE_TEST_MODE)
		  {
			  EE_writeLoadMode(BOOTLOADER_MODE);
		  }

		  HAL_Delay(5000);

	      JumpAddress = *(__IO uint32_t*) (USER_FLASH_FIRST_PAGE_ADDRESS + 4);
	      Jump_To_Application = (pFunction) JumpAddress;
	      /* Initialize user application's Stack Pointer */
	      __set_MSP(*(__IO uint32_t*) USER_FLASH_FIRST_PAGE_ADDRESS);
	      Jump_To_Application();
	      /* do nothing */
	      while(1);
	  }
	  /* Enter in IAP mode */
	  else
	  {
		  EE_writeLoadMode(MAIN_FIRMWARE_TEST_MODE);

//		  BSP_Config();

		  /* Initialize the LwIP stack */
		    lwip_init();
		    /* Configure the Network interface */
		    Netif_Config();
		#ifdef USE_IAP_HTTP
		    /* Initialize the webserver module */
		    IAP_httpd_init();
		#endif

		#ifdef USE_IAP_TFTP
		    /* Initialize the TFTP server */
		    IAP_tftpd_init();
		#endif
		    /* Notify user about the network interface config */
//		    User_notification(&gnetif);
		    /* Infinite loop */


		    while (1)
		    {
		    /* Read a received packet from the Ethernet buffers and send it
		       to the lwIP for handling */
		    ethernetif_input(&gnetif);

		    /* Handle timeouts */
		    sys_check_timeouts();

		#ifdef USE_DHCP
		    /* handle periodic timers for LwIP */
		    DHCP_Periodic_Handle(&gnetif);
		#endif
		    }
	  }
}


/* USART6 init function */
static void MX_USART6_UART_Init(void)
{
	huart6.Instance = USART6;
	huart6.Init.BaudRate = 115200;
	huart6.Init.WordLength = UART_WORDLENGTH_8B;
	huart6.Init.StopBits = UART_STOPBITS_1;
	huart6.Init.Parity = UART_PARITY_NONE;
	huart6.Init.Mode = UART_MODE_TX_RX;
	huart6.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart6.Init.OverSampling = UART_OVERSAMPLING_16;
	huart6.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	huart6.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	if (HAL_UART_Init(&huart6) != HAL_OK)
	{
		Error_Handler();
	}
}



/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;

    /**Configure the main internal regulator output voltage
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 25;
  RCC_OscInitStruct.PLL.PLLN = 432;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Activate the Over-Drive mode
    */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }

    /**Initializes the CPU, AHB and APB busses clocks
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7) != HAL_OK)
  {
    Error_Handler();
  }

  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_LTDC|RCC_PERIPHCLK_USART6;
  PeriphClkInitStruct.PLLSAI.PLLSAIN = 100;
  PeriphClkInitStruct.PLLSAI.PLLSAIR = 2;
  PeriphClkInitStruct.PLLSAI.PLLSAIQ = 2;
  PeriphClkInitStruct.PLLSAI.PLLSAIP = RCC_PLLSAIP_DIV2;
  PeriphClkInitStruct.PLLSAIDivQ = 1;
  PeriphClkInitStruct.PLLSAIDivR = RCC_PLLSAIDIVR_2;
  PeriphClkInitStruct.Usart6ClockSelection = RCC_USART6CLKSOURCE_PCLK2;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Configure the Systick interrupt time
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
}


/** Configure pins as
        * Analog
        * Input
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

	GPIO_InitTypeDef GPIO_InitStruct;

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOG_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_GPIOI_CLK_ENABLE();
	__HAL_RCC_GPIOK_CLK_ENABLE();
	__HAL_RCC_GPIOF_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOE_CLK_ENABLE();

	/* Configure pins to shutdown and turn ON external device (IVI, ...) */
    GPIO_InitStruct.Pin = GPIO_PIN_14;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_SET);

	/* Configure pins to shutdown and turn ON external device (cooler, ...) */
    GPIO_InitStruct.Pin = GPIO_PIN_15;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_RESET);


}

/**
  * @brief  Initializes the lwIP stack
  * @param  None
  * @retval None
  */
static void Netif_Config(void)
{ 
  ip_addr_t ipaddr;
  ip_addr_t netmask;
  ip_addr_t gw;


#ifdef USE_DHCP
  ip_addr_set_zero_ip4(&ipaddr);
  ip_addr_set_zero_ip4(&netmask);
  ip_addr_set_zero_ip4(&gw);
#else
  IP_ADDR4(&ipaddr,ipValue[0],ipValue[1],ipValue[2],ipValue[3]);
  IP_ADDR4(&netmask,maskValue[0],maskValue[1],maskValue[2],maskValue[3]);
//  IP_ADDR4(&ipaddr,IP_ADDR0,IP_ADDR1,IP_ADDR2,IP_ADDR3);
//  IP_ADDR4(&netmask,NETMASK_ADDR0,NETMASK_ADDR1,NETMASK_ADDR2,NETMASK_ADDR3);
  IP_ADDR4(&gw,GW_ADDR0,GW_ADDR1,GW_ADDR2,GW_ADDR3);
#endif /* USE_DHCP */
  
  /* remove net interface if exists */
  netif_remove(&gnetif);
  netif_add(&gnetif, &ipaddr, &netmask, &gw, NULL, &ethernetif_init, &ethernet_input);
  /*  Registers the default network interface. */
  netif_set_default(&gnetif);
  
  if (netif_is_link_up(&gnetif))
  {
    /* When the netif is fully configured this function must be called.*/
    netif_set_up(&gnetif);
  }
  else
  {
    /* When the netif link is down this function must be called */
    netif_set_down(&gnetif);
  }
}


///**
//  * @brief  Initializes the STM324xG-EVAL's LCD and LEDs resources.
//  * @param  None
//  * @retval None
//  */
//static void BSP_Config(void)
//{
//  GPIO_InitTypeDef GPIO_InitStructure;
//
//  /* Enable PB14 to IT mode: Ethernet Link interrupt */
//  __HAL_RCC_GPIOB_CLK_ENABLE();
//  GPIO_InitStructure.Pin = GPIO_PIN_14;
//  GPIO_InitStructure.Mode = GPIO_MODE_IT_FALLING;
//  GPIO_InitStructure.Pull = GPIO_NOPULL;
//  HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);
//
//  /* Enable EXTI Line interrupt */
//  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0xF, 0);
//  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
//
//  /* Configure LED1, LED2, LED3 and LED4 */
//  BSP_LED_Init(LED1);
//
//  /* Set Systick Interrupt to the highest priority */
//  HAL_NVIC_SetPriority(SysTick_IRQn, 0x0, 0x0);
//
//#ifdef USE_LCD
//
//  /* Initialize the STM324xG-EVAL's LCD */
//  BSP_LCD_Init();
//
//  /* Initialize LCD Log module */
//  LCD_LOG_Init();
//
//  /* Show Header and Footer texts */
//  LCD_LOG_SetHeader((uint8_t *)"Ethernet IAP Application");
//  LCD_LOG_SetFooter((uint8_t *)"STM324xG-EVAL board");
//
//  LCD_UsrLog("  State: Ethernet Initialization ...\n");
//
//#endif
//}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
static void Error_Handler(void)
{
  /* User may add here some code to deal with this error */
  while(1)
  {
  }
}

/**
  * @brief  Configure the MPU attributes as Device for  Ethernet Descriptors in the SRAM1.
  * @note   The Base Address is 0x20010000 since this memory interface is the AXI.
  *         The Configured Region Size is 256B (size of Rx and Tx ETH descriptors) 
  *       
  * @param  None
  * @retval None
  */
static void MPU_Config(void)
{
  MPU_Region_InitTypeDef MPU_InitStruct;
  
  /* Disable the MPU */
  HAL_MPU_Disable();
  
  /* Configure the MPU attributes as Device for Ethernet Descriptors in the SRAM */
  MPU_InitStruct.Enable = MPU_REGION_ENABLE;
  MPU_InitStruct.BaseAddress = 0x20010000;
  MPU_InitStruct.Size = MPU_REGION_SIZE_256B;
  MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
  MPU_InitStruct.IsBufferable = MPU_ACCESS_BUFFERABLE;
  MPU_InitStruct.IsCacheable = MPU_ACCESS_NOT_CACHEABLE;
  MPU_InitStruct.IsShareable = MPU_ACCESS_SHAREABLE;
  MPU_InitStruct.Number = MPU_REGION_NUMBER0;
  MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL0;
  MPU_InitStruct.SubRegionDisable = 0x00;
  MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_ENABLE;

  HAL_MPU_ConfigRegion(&MPU_InitStruct);

  /* Enable the MPU */
  HAL_MPU_Enable(MPU_PRIVILEGED_DEFAULT);
}


/* write and save load mode into flash
 * loadMode - value of load mode to save in flash */
void EE_writeLoadMode(FirmwareLoadMode loadMode) {

	uint16_t status;

	status = EE_ReadVariable(VirtAddVarTab[CONFIG_FIRMUPGRADE_FLASH_ORDER_NUMBER],  &VarDataTab[CONFIG_FIRMUPGRADE_FLASH_ORDER_NUMBER]);

	/* compare current and previous values of configuration to decide if we need to rewrite value or not */
	if (status != HAL_OK || loadMode != VarDataTab[CONFIG_FIRMUPGRADE_FLASH_ORDER_NUMBER])
	{
	/* rewrite value in flash with new value */
		EE_WriteVariable(VirtAddVarTab[CONFIG_FIRMUPGRADE_FLASH_ORDER_NUMBER],  loadMode);
	}

}

/* load configuration values from flash into table objects (TableMain, TableConfig) */
FirmwareLoadMode EE_readLoadMode(void) {

	FirmwareLoadMode loadMode = BOOTLOADER_MODE;

	EE_ReadVariable(VirtAddVarTab[CONFIG_FIRMUPGRADE_FLASH_ORDER_NUMBER],  &VarDataTab[CONFIG_FIRMUPGRADE_FLASH_ORDER_NUMBER]);

	loadMode = VarDataTab[CONFIG_FIRMUPGRADE_FLASH_ORDER_NUMBER];

	return loadMode;
}


/**
  * @brief  CPU L1-Cache enable.
  * @param  None
  * @retval None
  */
static void CPU_CACHE_Enable(void)
{
  /* Enable I-Cache */
  SCB_EnableICache();

  /* Enable D-Cache */
  SCB_EnableDCache();
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
