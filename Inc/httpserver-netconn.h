#ifndef __HTTPSERVER_NETCONN_H__
#define __HTTPSERVER_NETCONN_H__

#include "lwip/def.h"
#include "fsdata.h"




struct fs_file {
  char *data;
  int len;
};

void IAP_httpd_init(void);

#endif /* __HTTPSERVER_NETCONN_H__ */
